import QtQuick 2.0

Component {
    id: highlightBar
    Rectangle {
        visible: gridView.focus
        opacity: 0.2
        gradient: Gradient {
            GradientStop { position: 0.0; color: "grey" }
            GradientStop { position: 1.0; color: "white" }
        }
        border {
            color: "White"
            width: 3
        }

        radius: 20
        width: gridView.cellWidth - 20
        height: gridView.cellHeight - 20

        x: gridView.currentItem.x;
            Behavior on x { SpringAnimation { spring: 2; damping: 0.1 } }
        y: gridView.currentItem.y;
            Behavior on y { SpringAnimation { spring: 2; damping: 0.1 } }
    }
}
