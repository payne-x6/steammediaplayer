import QtQuick 2.9
import QtMultimedia 5.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0

Item {
    property url source
    onSourceChanged: mediaPlayer.source = source

    function pause() {
        mediaPlayer.pause()
    }

    function play() {
        if(mediaPlayer.hasAudio | mediaPlayer.hasVideo) {
            mediaPlayer.play()
        }
    }

    onActiveFocusChanged: if(activeFocus) playButton.forceActiveFocus()

    focus: true

    Rectangle {
        anchors.fill: parent
        color: "black"

        Image {
            visible: !mediaPlayer.hasVideo
            anchors.fill: parent
            source: "images/logo.png"
        }

        MediaPlayer {
            id: mediaPlayer
            autoPlay: true

            onPlaying: mainWindow.visibleUi(false)
            onPaused: mainWindow.visibleUi(true)
            onStopped: mainWindow.visibleUi(true)

        }



        VideoOutput {
            anchors.fill: parent
            source: mediaPlayer
            //opacity: 0.2

            fillMode: "PreserveAspectFit"
        }

        Timer {
            id: hideTimer
            interval: 3000
            onTriggered: {
                videoMouseArea.cursorShape = Qt.BlankCursor
            }
        }

        MouseArea {
            id: videoMouseArea
            hoverEnabled: true
            anchors.fill: parent
            onPositionChanged: {
                hideTimer.restart()
                videoMouseArea.cursorShape = Qt.CustomCursor
            }
        }

    }


    RowLayout {
        anchors {
            horizontalCenter: parent.horizontalCenter;
            bottom: parent.bottom;
            bottomMargin: 20
        }



        Button {
            id: playButton

            flat: true
            focus: true
            enabled: mediaPlayer.hasAudio | mediaPlayer.hasVideo
            //Layout.preferredWidth: playButton.implicitHeight
            icon.name: mediaPlayer.playbackState === MediaPlayer.PlayingState ? "Pause" : "Play"
            icon.source: mediaPlayer.playbackState === MediaPlayer.PlayingState ? "images/pause-16.png" : "images/play-16.png"

            Keys.onReturnPressed: mediaPlayer.playbackState === MediaPlayer.PlayingState ? mediaPlayer.pause() : mediaPlayer.play()
            Keys.onEscapePressed: popup.open()
            KeyNavigation.up: bar.currentItem

            MouseArea {
                id: playArea
                anchors.fill: parent
                acceptedButtons: Qt.LeftButton
                onPressed: mediaPlayer.playbackState === MediaPlayer.PlayingState ? mediaPlayer.pause() : mediaPlayer.play()
            }

            RadialGradient {
                id: playButtonGlow
                visible: playButton.activeFocus
                anchors.fill: parent
                gradient: Gradient {
                    GradientStop { position: 0.0; color: "#5555ff" }
                    GradientStop { position: 0.5; color: "transparent" }
                }
           }
        }
    }
}
