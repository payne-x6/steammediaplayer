import QtQuick 2.9
import QtQuick.Controls 2.3
import QtGraphicalEffects 1.0

Popup {
    id: popup

    parent: Overlay.overlay
    width: parent.width * 3 / 4
    height: parent.height * 3 / 4

    focus: true
    modal: true

    x: Math.round((parent.width - width) / 2)
    y: Math.round((parent.height - height) / 2)

    onOpened: {
        cancelButton.forceActiveFocus(Qt.PopupFocusReason)
    }

    background: RadialGradient {
        horizontalRadius: height/2
        verticalRadius: height/2
        horizontalOffset: width/4
        verticalOffset: -height/4
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#415475" }
            GradientStop { position: 1.0; color: "#0d293c" }
        }
    }

    Column {
        anchors.fill: parent


        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Application is about to exit."
            color: "white"
            font {
                pointSize: 20
            }
        }
        Text {
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Do you really want to exit application?"
            color: "white"
        }
    }
    Row {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        spacing: 20
        padding: 20




        Button {
            id: cancelButton
            text: "No"
            activeFocusOnTab: true
            focus: true

            background: Item {
                anchors.fill: parent
                RectangularGlow {
                    id: cancleBackgroundGlow
                    visible: cancelButton.activeFocus
                    anchors.fill: parent
                    glowRadius: 10
                    spread: 0
                    color: "#5555ff"
                    cornerRadius: 15
                }
                Rectangle {
                    anchors.fill: parent
                    id: backgroundRect
                    //opacity: 0.2
                    color: "#415475"
                }
            }

            onClicked: popup.close()
            Keys.onReturnPressed: popup.close()
            Keys.onEnterPressed: popup.close()

            KeyNavigation.right: exitButton
            KeyNavigation.left: exitButton
        }
        Button {
            id: exitButton
            text: "Exit"

            background: Item {
                anchors.fill: parent
                RectangularGlow {
                    id: exitBackgroundGlow
                    visible: exitButton.activeFocus
                    anchors.fill: exitBackgroundRect

                    glowRadius: 10
                    spread: 0
                    color: "grey"
                    cornerRadius: 15
                }
                Rectangle {
                    anchors.fill: parent
                    id: exitBackgroundRect
                    //opacity: 0.2
                    color: "grey"
                }
            }



            onClicked: Qt.quit()
            Keys.onReturnPressed: Qt.quit()
            Keys.onEnterPressed: Qt.quit()

            KeyNavigation.right: cancelButton
            KeyNavigation.left: cancelButton
        }
    }
}

