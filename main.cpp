#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QTimer>

#include "SteamAPIWrapper.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<SteamAPIWrapper>("cz.hak.jan.steammediaplayer.steamapi", 1, 0, "SteamAPI");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
