import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.folderlistmodel 2.2
import Qt.labs.platform 1.0
import QtGraphicalEffects 1.0

import cz.hak.jan.steammediaplayer.steamapi 1.0

ApplicationWindow {
    id: mainWindow
    visible: true
    visibility: "FullScreen"
    width: 640
    height: 480
    title: qsTr("Steam Media Player")

    signal visibleUi(bool uiVisible)

    onVisibleUi: {
        bar.visible = uiVisible
    }

    SteamAPI {
        id: steam
    }

    background: RadialGradient {
        horizontalRadius: height/2
        verticalRadius: height/2
        horizontalOffset: width/4
        verticalOffset: -height/4
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#415475" }
            GradientStop { position: 1.0; color: "#0d293c" }
        }
    }


    header: TabBar {
        id: bar
        opacity: 0.2
        TabButton {
            id: browseTab
            text: qsTr("Browse")
        }
        TabButton {
            text: qsTr("Library")
        }
        TabButton {
            id: playerTab
            text: qsTr("Player")
        }

        KeyNavigation.down: gridView
        Keys.onEscapePressed: {
            popup.open()
        }
    }

    StackLayout {
        id: stackView
        anchors.fill: parent
        currentIndex: bar.currentIndex
        onCurrentIndexChanged: {
            if(currentIndex == 2) {
                mediaPlayer.play()
                mediaPlayer.forceActiveFocus()
            }
            else {
                mediaPlayer.pause()
            }
        }

        Item {
            GridView {
                id: gridView
                anchors.fill: parent
                focus: true

                cellWidth: Math.max(gridView.width / 6,  100)
                cellHeight: cellWidth + 25

                model: FolderListModel {
                    id: folderModel
                    folder: StandardPaths.standardLocations(StandardPaths.MoviesLocation)[0]
                    sortField: "Name"
                    showDirs: true
                    showDirsFirst: true
                    showDotAndDotDot: true
                    nameFilters: [
                            "*.aa",     "*.aac",    "*.aax",    "*.act",    "*.aiff",   "*.amr",    "*.amv",    "*.ape",
                            "*.asf",    "*.au",     "*.avi",    "*.awb",    "*.dct",    "*.drc",    "*.dss",    "*.dvf",
                            "*.flac",   "*.flv",    "*.f4a",    "*.f4b",    "*.f4p",    "*.f4v",    "*.gif",    "*.gifv",
                            "*.gsm",    "*.iklax",  "*.ivs",    "*.mkv",    "*.mmf",    "*.mogg",   "*.mov",    "*.mpc",
                            "*.mpe",    "*.mpeg",   "*.mpg",    "*.mpv",    "*.mp2",    "*.mp3",    "*.mp4",    "*.msv",
                            "*.mxf",    "*.m2v",    "*.m4a",    "*.m4b",    "*.m4p",    "*.m4v",    "*.nsf",    "*.nsv",
                            "*.oga",    "*.ogg",    "*.ogv",    "*.opus",   "*.qt",     "*.ra",     "*.raw",    "*.rm",
                            "*.rmvb",   "*.roq",    "*.sln",    "*.svi",    "*.tta",    "*.vob",    "*.vox",    "*.wav",
                            "*.webm",   "*.wma",    "*.wmv",    "*.wv",     "*.yuv",    "*.3gp",    "*.3g2",    "*.8svx"
                        ]
                }

                delegate: GridViewElement {}

                highlight: GridViewHighlight {}
                highlightFollowsCurrentItem: false
                interactive: true

                keyNavigationEnabled: true
                KeyNavigation.up: bar.currentItem

                Keys.onEscapePressed: {
                    popup.open()
                }
            }
        }
        Item {
            Text {
                text: "Comming Soon"
                color: "White"
                anchors.fill: parent
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
            }
        }
        MediaPlayerWidget {
            id: mediaPlayer
        }

        ExitPopup {
            id: popup
        }
    }
}

