#ifndef STEAMAPIWRAPPER_H
#define STEAMAPIWRAPPER_H

#include <QObject>


#include "include/steam/steam_api.h"
#include "include/steam/isteamfriends.h"

class SteamAPIWrapper : public QObject
{
    Q_OBJECT

public:
    explicit SteamAPIWrapper(QObject *parent = nullptr);
    void showOverlay();

signals:

public slots:
};

#endif // BACKEND_H
