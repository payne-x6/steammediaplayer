# SteamMediaPlayer

 - [About](#markdown-header-about)
 - [Downloads](#markdown-header-downloads)
 - [Support project](#markdown-header-support-project)
 - [Known Issues](#markdown-header-known-issues)
 
## About 
 
Steam Media Player is lightweight audio and video media player for Steam. It's made especially for Steam Big Picture mode and Steam Link, using Keyboard/Gamepad controlls to navigate.

![Browse your files](https://i.imgur.com/elxBqAp.png)
![Play your video or audio library](https://i.imgur.com/m6YOwXi.png)

### Milestones

**Milestone #1**

Stable version of software for Windows and Linux, that allows user to play videos from his PC and manage Library of films.

**Milestone #2**

Get software on Steam Store. Include alternative sources of media (DVD, Network disks, etc.). 

**Milestone #3**

Stable MacOS version. Steam Link version, that allows you stream films from PC without need to run media player on hosting PC.

## Downloads

**version 0.1 (Unstable / Development version)**

| Version \ Platform | Windows | Linux | MacOs | Steam Link |
| -------- | ------- | ----- | ----- | ---------- |
| 0.1 (x64 architecture) (Development version)  | [Steam Media Player v0.1 Windows](https://bitbucket.org/payne-x6/steammediaplayer/downloads/SteamMediaPlayer.zip) | Steam Media Player v0.1 Linux | --- | --- |


## Support project

###Donate

Even small donation keeps me update this project. There is lot of things to do, and one of them is get 1.000 EUR to get this product on the Steam Store (licence for publish on Steam Store cost 1.000 EUR).

[Paypal.me](https://www.paypal.me/juspayne69)

###Contribute

Even you can join this project. Every hand is welcome. We are looking for:

 - Graphic designers
 - Programmers
 - Documentation writers

## Known Issues

**Steam Media Player does not support controller settings at Big Picture app icon!**

Right now, there will not be way, to publish this application on the Steam store. They do not accept other applications (except few categories where this application doesn't fit).
Due it this application doesn't get full Steam support (Steam Product ID) and then they not show your application as application which support controllers. In application, support of controller is. Don't worry.

**Steam overlay not wotking**

When you open Steam Overlay keys will not be listened. In fact, they are listened in application instead of overlay (if you opened it nothing happends, just close it).. Just do not use overlay right now.
It will be resolved once when this application gets Steam Product ID.
 
