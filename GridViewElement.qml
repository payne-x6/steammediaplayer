import QtQuick 2.0

Component {
    id: listViewDelegate
    Item {
        width: gridView.cellWidth
        height: gridView.cellHeight
        Column {
            Image {
                source: {
                    if(fileName == ".") {
                        source = "images/folder-all.png"
                    }
                    else if(fileName == "..") {
                        source = "images/folder-up.png"
                    }
                    else {
                        source = "images/folder.png"
                    }

                }
                width: gridView.cellWidth - 20
                height: gridView.cellWidth - 20
                anchors.horizontalCenter: parent.horizontalCenter
            }
            Text {
                text: fileName
                color: "White"
                anchors.horizontalCenter: parent.horizontalCenter
                width: gridView.cellWidth - 20
                elide: Text.ElideRight
                horizontalAlignment: Text.AlignHCenter

            }
        }
        Keys.onEnterPressed: {
            if(fileIsDir) {
                folderModel.folder = fileURL
            }
            else if(!fileIsDir) {
                bar.currentIndex = 2
                mediaPlayer.source = fileURL
            }
        }
        Keys.onReturnPressed: {
            if(fileIsDir) {
                folderModel.folder = fileURL
            }
            else if(!fileIsDir) {
                bar.currentIndex = 2
                mediaPlayer.source = fileURL
            }
        }

        MouseArea {
            anchors.fill: parent;
            onClicked: {
                if(gridView.currentIndex == index) {
                    if(fileIsDir) {
                        folderModel.folder = fileURL
                    }
                    else if(!fileIsDir) {
                        bar.currentIndex = 2
                        mediaPlayer.source = fileURL
                    }

                    return
                }

                gridView.currentIndex = index
            }
        }
    }
}
